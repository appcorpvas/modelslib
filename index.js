

module.exports = function (host, port, db) {


const options = {
  //useMongoClient: true,
  autoIndex: false, // Don't build indexes
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 150, // Maintain up to 10 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  connectTimeoutMS: 1000000, // Give up initial connection after 10 seconds
  socketTimeoutMS: 60000, // Close sockets after 45 seconds of inactivity
  useNewUrlParser: true,
  autoReconnect: true
};


    var fs = require('fs')
    var mongoose = require("mongoose");
    mongoose.Promise = global.Promise; //this line to avoid mongoose deprecated promises warnings
    mongoose.connect(`mongodb://${host}:${port}/${db}`, options);
    fs.readdirSync( __dirname + "/models").forEach(function (file) {
        require( __dirname + "/models/" + file);
    });
    SubscriberModel = mongoose.model("Subscriber")
    smsMsgsModel = mongoose.model("smsMsgs")
    smsInModel = mongoose.model("SMSIns")
    SubscriberQuestionsModel = mongoose.model("SubscriberQuestions")
    QuestionsModel  = mongoose.model("Questions")
    contentOutModel = mongoose.model("contentOut")
    ChargingModel = mongoose.model("Charging")
    KannelErrorModel = mongoose.model("kannelerror")
    bulkOutModel = mongoose.model("bulkOuts")
    reminderLogsModule = mongoose.model("reminderlogs")
    
    models = {SubscriberModel,
    smsMsgsModel,
    smsInModel,
    SubscriberQuestionsModel,
    QuestionsModel,
    contentOutModel,
    ChargingModel,
    KannelErrorModel,
    bulkOutModel,
    reminderLogsModule}

    return models
}
