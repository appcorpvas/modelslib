var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var Subscriber = new Schema({
    MSISDN: {
        type: String,
        index: true
    },
    startDate: {
        type: Date,
        default: Date.now
    },
    unSubDate: {
        type: Date
    },
    channel1Charged: {
        type: Boolean,
        default: false
    },
    channel2Charged: {
        type: Boolean,
        default: false
    },
    channel1LastCharged: {
        type: Date,
        index: true
    },
    channel2LastCharged: {
        type: Date,
        index: true
    },
    subCommand: {
        type: String,
        index: true
    },
    subStatus: {
        type: Boolean,
        default: true
    },
    source: {
        type: String,
        index: true
    },
    sessionId: {
        type: String,
        default: ""
    },
    giftIsSent: {
        type: Boolean,
        default: false
    },
    wip: {
        type: Boolean,
        default: false
    },
    lastActionAt: {
        type: Date,
        index: true
    },
    trials: {
        type: Number,
        default: 0,
        index: true
    },
    channel1SucceededCharge: {
        type: Number,
        default: 0
    },
    channel2SucceededCharge: {
        type: Number,
        default: 0
    },
    points: {
        type: Number,
        default: 100
    },
    customURL: {
        type: String,
        default :"https://goo.gl/NwFZ5i"
    },
    lastSalefny: {
        type: Date,
        index: true,
        default: null
    },
    blackListed: {
        type: Boolean,
        default: false,
        index: true
    },
    blackListDate: {
        type: Date,
        index: true,
        default: null
    },
    blackListReason: {
        type: String
    },
    promoCode: {
        type: String
    }
}, {
    minimize: false
})

mongoose.model("Subscriber", Subscriber)