var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var kannelerror = new Schema({
    text: {
        type: String,
        index: true
    },
    timeStamp: {
        type: Date,
        default: Date.now
    },
    from: {
        type : String
    },
    to: {
        type : String
    },
    smsc: {
        type : String
    },
}, {
        minimize: false
    })

    mongoose.model("kannelerror", kannelerror)