var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var bulkOuts = new Schema({
    sendDate: {
        type: Date,
    },
    text: {
        type: String,
        index: true
    },
    from: {
        type: String,
        index: true
    },
    to: {
        type: String,
        index: true
    },
    sentDate: {
        type: Date
    },
    smsc: {
        type: String
    },
    flashSMS: { 
        type: Boolean
    }
}, {
        minimize: false
    })

    mongoose.model("bulkOuts", bulkOuts)