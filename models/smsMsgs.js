var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var smsMsgs = new Schema({
    msgName: {
        type: String
    },
    msgText: {
        type: String
    }
}, {
    minimize: false
})

mongoose.model("smsMsgs", smsMsgs)