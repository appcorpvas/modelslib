var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var contentOutbox = new Schema({
    sendDate: {
        type: Date,
        index: true
    },
    text: {
        type: String,
        index: true
    },
    from: {
        type: String,
        index: true
    },
    to: {
        type: String,
        index: true
    },
    sentDate: {
        type: Date
    }
}, {
        minimize: false
    })

    mongoose.model("contentOutbox", contentOutbox)