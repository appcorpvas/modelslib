var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var SubscriberQuestions = new Schema({
    MSISDN: {
        type: String,
        index: true
    },
    questDate: {
        type: Date,
        default: Date.now
    },
    questId: {
        type: Object,
        default: 0
    },
    answer: {
        type: Number,
        default: 0
    },
    serviceId: {
        type: Number,
        default: 0
    },
    answeredAt: {
        type: Date,
        default: null
    }
}, {
    minimize: false
})

mongoose.model("SubscriberQuestions", SubscriberQuestions)