var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var contentOut = new Schema({
    packageId: {
        type: Number,
        default: 0
    },
    text: {
        type: String,
        index: true
    },
    isRead: {
        type: Boolean,
        default: false
    },
    readOn: {
        type: Date
    },
    wip: {
        type: Boolean,
        default: false
    },
    wroteOn: {
        type: Date,
        default: Date.now
    }
}, {
        minimize: false
    })

    mongoose.model("contentOut", contentOut)