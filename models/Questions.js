var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var Questions = new Schema({
    question: {
        type: String
    },
    answer: {
        type: Number
    },
    serviceId: {
        type: Number,
        default: 0
    }
}, {
    minimize: false
})

mongoose.model("Questions", Questions)