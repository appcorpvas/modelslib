var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var SMSIns = new Schema({
    insertDate: {
        type: Date,
        default: Date.now
    },
    text: {
        type: String,
        index: true
    },
    from: {
        type: String,
        index: true
    },
    to: {
        type: String,
        index: true
    },
    charset: {
        type: String
    },
    coding: {
        type: String
    },
    isRead: {
        type: Boolean,
        default: false
    },
    readOn: {
        type: Date
    },
    wip: {
        type: Boolean,
        default: false
    },
    smsc: {
        type: String,
        default: ""
    }
}, {
        minimize: false
    })

    mongoose.model("SMSIns", SMSIns)